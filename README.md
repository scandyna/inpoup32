
# Original

https://highrez.co.uk/

# Build

```Shell
mkdir $SomePathToABuildDir/Debug
cd $SomePathToABuildDir/Debug
cmake -G"MinGW Makefiles" -DCMAKE_BUILD_TYPE=Debug $PathToSource
cmake-gui .

mingw32-make
```
