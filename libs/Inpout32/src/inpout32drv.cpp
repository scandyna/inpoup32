/***********************************************************************\
*                                                                       *
* InpOut32drv.cpp                                                       *
*                                                                       *
* The entry point for the InpOut DLL                                    *
* Provides the 32 and 64bit implementation of InpOut32 DLL to install   *
* and call the appropriate driver and write directly to hardware ports. *
*                                                                       *
* Written by Phillip Gibbons (Highrez.co.uk)                            *
* Based on orriginal, written by Logix4U (www.logix4u.net)              *
*                                                                       *
\***********************************************************************/
#include "inpout32.h"
#include "hwinterfacedrv.h"
